defmodule SiteMonitor.Db.Repo.Migrations.EnableHstore do
  use Ecto.Migration

  def up do
    execute "CREATE EXTENSION hstore"
  end

  def down do
    execute "DROP EXTENSION hstore"
  end
end
