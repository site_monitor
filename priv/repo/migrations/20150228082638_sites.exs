defmodule SiteMonitor.Db.Repo.Migrations.Sites do
  use Ecto.Migration

  def up do
    create table(:sites) do
      add :name, :string, null: false
      add :url, :string, null: false
      add :port, :integer, null: false, default: 80
      add :method, :string, null: false, default: "get"
      add :username, :string
      add :password, :string
      add :post_data, :string
      add :interval, :integer, null: false, default: 1
      add :expected_status, :integer, null: false, default: 200

      timestamps
    end
  end

  def down do
    drop table(:sites)
  end
end
