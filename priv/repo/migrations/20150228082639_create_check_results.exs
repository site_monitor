defmodule SiteMonitor.Db.Repo.Migrations.CreateCheckResults do
  use Ecto.Migration

  def up do
    create table(:check_results) do
      add :site_id, references(:sites)
      add :status, :integer, null: false
      add :headers, :hstore, null: false, default: ""
      add :body, :text, default: ""
      add :response_time, :integer, null: false

      timestamps
    end
  end

  def down do
    drop table(:check_results)
  end
end
