defmodule SiteMonitor.Logger do
  use GenEvent

  require Logger

  ## GenEvent Callbacks

  def handle_event({:create, site_id, _pid}, state) do
    Logger.info "create site: #{site_id}"
    {:ok, state}
  end

  def handle_event({:exit, site_id, _pid}, state) do
    Logger.info "exited site: #{site_id}"
    {:ok, state}
  end

  def handle_event({:site_init, site_id, interval}, state) do
    Logger.info "site initialized: #{site_id}, interval: #{interval}"
    {:ok, state}
  end

  def handle_event({:check_start, site_id, _pid}, state) do
    Logger.info "site check started site: #{site_id}"
    {:ok, state}
  end
  
  def handle_event({:check_finish, site_id, check_id, _pid}, state) do
    Logger.info "site heck finished site: #{site_id} check: #{check_id}"
    {:ok, state}
  end
end
