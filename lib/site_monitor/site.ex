defmodule SiteMonitor.Site do
  use GenServer
  
  require Logger

  alias SiteMonitor.Db

  @doc """
  Starts monitoring a site
  """
  def start_link(event_manager, db, site_id, interval, opts \\ []) do
    GenServer.start_link(__MODULE__, {event_manager, db, site_id, interval}, opts)
  end

  require Logger

  @doc """
  Starts a site process check loop.
  """
  def start_check(site) do
    send site, {:check, 0}
  end

  @doc """
  Stops a site.
  """
  def stop(site) do
    GenServer.call(site, :stop)
  end

  ## Server callbacks
  
  def init({event_manager, db, site_id, interval}) do
    GenEvent.notify(event_manager, {:site_init, site_id, interval})

    {:ok, %{events: event_manager, db: db, site_id: site_id, interval: interval, count: 0}}
  end

  # TODO: Handle site interval update.

  def handle_call(:stop, _from, state) do
    {:stop, :normal, :ok, state}
  end

  def handle_info({:check, count}, state) do
    GenEvent.notify(state.events, {:check_start, state.site_id, self})

    check = SiteMonitor.HttpCheck.async(state.db, state.site_id)

    trigger_check(state.interval, count + 1) 

    check = Task.await(check)

    GenEvent.notify(state.events, {:check_finish, state.site_id, check.id, self})

    {:noreply, %{state | count: count + 1}}
  end

  defp trigger_check(interval, count) do
    :erlang.send_after(interval, self, {:check, count})
  end
end
