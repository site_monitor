defmodule SiteMonitor.Db do
  defmodule Repo do
    use Ecto.Repo, otp_app: :site_monitor, adapter: Ecto.Adapters.Postgres
  end

  defmodule Model do
    @doc """
    Creates an instance of `model` given its `attributes`.
    """
    def instanciate(model, attributes) do
      struct(model, attributes |> Enum.chunk(2) 
                               |> Enum.map(fn([k, v]) -> {String.to_existing_atom(k), v} end)
                               |> Enum.into(%{}))
    end

    @doc """
    Tests wether a model has been persisted, this is implemented by testing for a non nil id.
    """
    def persisted?(model), do: model.id != nil

    @doc """
    Returns a list usable with the Redis HMSET command. Rejects nil values and :__struct__.
    """
    def as_hmset(model) do
      model |> Map.to_list 
            |> Enum.reject(fn({k, v}) -> k == :__struct__ || v == nil end) 
            |> Enum.flat_map(fn({k, v}) -> [Atom.to_string(k), v] end) 
    end
  end

  defmodule CheckResult do
    use Ecto.Model

    schema "check_results" do
      field :status, :integer
      field :headers, Ecto.Hstore
      field :body, :string
      field :response_time, :integer
      field :created_at, :datetime

      belongs_to :site, SiteMonitor.Db.Site

      timestamps
    end

    def changeset(result, params \\ nil) do
      params 
        |> cast(result, ~w(site_id status response_time), ~w(body headers))

    end

  end

  defmodule Site do
    use Ecto.Model

    @valid_intervals [1, 5, 15, 30, 60]
    @valid_methods ["get", "post", "put", "head", "options", "patch"]

    schema "sites" do
      field :name, :string
      field :url, :string
      field :port, :integer, default: 80
      field :method, :string, default: "get"
      field :username, :string
      field :password, :string
      field :post_data, :string
      field :interval, :integer, default: 1
      field :expected_status, :integer, default: 200

      has_many :check_results, SiteMonitor.Db.CheckResult

      timestamps
    end

    def changeset(site, params \\ nil) do
      params 
        |> cast(site, ~w(name url), ~w(port method username password post_data interval expected_status))
        |> validate_inclusion(:method, @valid_methods)
        |> validate_inclusion(:interval, @valid_intervals)
    end

    @doc """
    Returns a site interval in milliseconds.
    """
    def interval_in_millis(%Site{interval: interval}), do: interval * 60000
  end

  import Ecto.Query

  @doc """
  Starts the database agent using the given redis url.
  """
  def start_link(repo, opts \\ []) do
    Agent.start_link(fn -> repo end, opts)
  end

  @doc """
  Creates a new site in the database.
  """
  @spec create_site(pid, Site.t):: :error | {:ok, Site.t}
  def create_site(db, site) do
    Agent.get(db, fn(repo) -> 
      changeset = Site.changeset(%Site{}, %{name: site.name, url: site.url})

      if changeset.valid? do
        {:ok, repo.insert(site)}
      else
        IO.puts inspect(changeset.errors)
        :error
      end
    end)
  end

  @doc """
  List all sites in the database.
  """
  def list_sites(db) do
    Agent.get(db, fn(repo) ->
      try do
        repo.all(from s in Site, select: s)
      rescue
        e -> []
      end
    end)
  end

  @doc """
  Finds a site by it's ID.
  """
  def find_site(db, id) when is_nil(id), do: nil
  def find_site(db, id) do
    Agent.get(db, fn(repo) ->
      repo.get Site, id
    end)
  end

  @doc """
  Creates a check result for a site.
  """
  def create_check_result(site_id, status, headers, body) do
  end

  @doc """
  Lists all check results of a given site.
  """
  def list_check_results(site_id) do
  end

  @doc """
  Finds the check result of a site.
  """
  def find_check_result(id) do
  end
end

