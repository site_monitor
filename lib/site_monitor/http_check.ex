defmodule SiteMonitor.HttpCheck do
  alias SiteMonitor.Db

  require Logger

  @doc """
  Runs an HttpCheck task
  """
  def async(db, site_id) do
    Task.async(__MODULE__, :check, [db, site_id])
  end

  def check(db, site_id) do
    site = Db.find_site(db, site_id)
      
    start = :erlang.now()

    response = HTTPoison.get(site.url)

    elapsed = :timer.now_diff(start, :erlang.now())

    create_result(response, elapsed)
  end

  def create_result(res, elapsed_time) do
    case res do
      {:ok, response} ->
        Logger.info "response: #{inspect response}"
      {:error, error} ->
        Logger.info "error: #{inspect error}"
    end

    %{id: 1}
  end
end
