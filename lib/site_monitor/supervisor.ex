defmodule SiteMonitor.Supervisor do
  use Supervisor

  def start_link do
    Supervisor.start_link(__MODULE__, :ok)
  end

  @event_manager_name SiteMonitor.EventManager
  @sites_supervisor_name SiteMonitor.Supervisor
  @registry_name SiteMonitor.Registry
  @db_name SiteMonitor.Db

  def init(:ok) do
    children = [
      worker(SiteMonitor.Db.Repo, []),
      worker(SiteMonitor.Db, [SiteMonitor.Db.Repo, [name: @db_name]]),
      worker(GenEvent, [[name: @event_manager_name]]),
      supervisor(SiteMonitor.Site.Supervisor, [[name: @sites_supervisor_name]]), 
      worker(SiteMonitor.Registry, [@db_name, @event_manager_name, @sites_supervisor_name, [name: @registry_name]])
    ]

    supervise(children, strategy: :one_for_one)
  end
end
