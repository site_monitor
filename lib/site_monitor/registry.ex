defmodule SiteMonitor.Registry do
  use GenServer

  alias SiteMonitor.Db
  alias SiteMonitor.Site

  @doc """
  Starts the registry
  """
  def start_link(db, event_manager, sites, opts \\ []) do
    GenServer.start_link(__MODULE__, {db, event_manager, sites}, opts)
  end

  @doc """
  Looks up the registry pid for `id` stored in `server`

  Returns `{:ok, pid}` if the site exists, `:error` otherwise.
  """
  def lookup(server, id) do
    GenServer.call(server, {:lookup, id})
  end

  @doc """
  Ensures there is a site associated to `id` in `server`
  """
  def create(server, site) do
    GenServer.call(server, {:create, site})
  end

  @doc """
  Stops the registry
  """
  def stop(registry) do
    GenServer.call(registry, :stop)
  end

  ## Server callbacks

  def init({db, event_manager, sites_sup}) do
    GenEvent.add_mon_handler(event_manager, SiteMonitor.Logger, self)

    sites = Enum.map(Db.list_sites(db), fn(site) -> 
      {:ok, pid} = Site.Supervisor.start_site(sites_sup, event_manager, db, site.id, Db.Site.interval_in_millis(site))

      SiteMonitor.Site.start_check pid

      ref = Process.monitor(pid)
      {site.id, ref, pid}
    end)

    ids = Enum.reduce(sites, HashDict.new, fn({id, _ref, pid}, acc) ->
      HashDict.put(acc, id, pid)
    end)

    refs = Enum.reduce(sites, HashDict.new, fn({id, ref, _pid}, acc) ->
      HashDict.put(acc, ref, id)
    end)

    {:ok, %{ids: ids, refs: refs, db: db, events: event_manager, sites: sites_sup}}
  end

  def handle_call({:lookup, id}, _from, state) do
    {:reply, HashDict.fetch(state.ids, id), state}
  end

  defp registered?(state, site_id), do: HashDict.has_key?(state.ids, site_id)

  def handle_call({:create, site}, _from, state) do
    if registered?(state, site.id) do
      {:reply, HashDic.fetch(state.ids, site.id), state}
    else
      case Db.create_site(state.db, site) do
        {:ok, site} ->
          {:ok, pid} = Site.Supervisor.start_site(state.sites, state.events, state.db, site.id, Db.Site.interval_in_millis(site))

          SiteMonitor.Site.start_check pid

          ref = Process.monitor(pid)
          refs = HashDict.put(state.refs, ref, site.id)
          ids = HashDict.put(state.ids, site.id, pid)

          GenEvent.notify(state.events, {:create, site.id, pid})

          {:reply, {:ok, site.id, pid}, %{state | refs: refs, ids: ids}};
        :error ->
          {:reply, :error, state}
      end
    end
  end

  def handle_call(:stop, _from, state) do
    {:stop, :normal, :ok, state}
  end

  def handle_info({:DOWN, ref, :process, pid, _reason}, state) do
    {id, refs} = HashDict.pop(state.refs, ref)

    ids = HashDict.delete(state.ids, id)

    GenEvent.notify(state.events, {:exit, id, pid})

    {:noreply, %{state | ids: ids, refs: refs}}
  end

  def handle_info(_msg, state) do
    {:noreply, state}
  end
end
