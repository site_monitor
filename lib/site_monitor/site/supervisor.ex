defmodule SiteMonitor.Site.Supervisor do
  use Supervisor

  def start_link(opts \\ []) do
    Supervisor.start_link(__MODULE__, :ok, opts)
  end

  def start_site(supervisor, event_manager, db, site_id, interval) do
    Supervisor.start_child(supervisor, [event_manager, db, site_id, interval])
  end

  def init(:ok) do
    children = [
      worker(SiteMonitor.Site, [], restart: :temporary)
    ]

    supervise(children, strategy: :simple_one_for_one)
  end
end
