defmodule SiteMonitor do
	use Application
	
	def start(_type, _args) do
		SiteMonitor.Supervisor.start_link
	end
end
