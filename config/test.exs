use Mix.Config

config :logger, :console,
       level: :warn

config :site_monitor, SiteMonitor.Db.Repo,
  adapter: Ecto.Adapters.Postgres,
  database: "site_monitor_test",
  username: "paul",
  hostname: "localhost"
