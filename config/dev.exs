use Mix.Config

config :site_monitor, SiteMonitor.Db.Repo,
  adapter: Ecto.Adapters.Postgres,
  database: "site_monitor_dev",
  username: "paul",
  hostname: "localhost"
