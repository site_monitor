defmodule SiteMonitor.DbTest do
  use ExUnit.Case, async: true

  alias SiteMonitor.Db

  setup do
    {:ok, db} = Db.start_link("redis://127.0.0.1:6370/8")

    on_exit :exredis, fn ->
      Exredis.start_using_connection_string("redis://127.0.0.1:6379/8")
        |> Exredis.query ["FLUSHALL"]
    end

    {:ok, db: db}
  end

  test "create a site", %{db: db} do
    assert {:ok, site} = Db.create_site(db, %Db.Site{name: "example", url: "http://example.com"})
    assert site.id != nil
    assert is_integer(site.id)
  end
end
