defmodule RegistryTest do
  use ExUnit.Case, async: true

  defmodule Forwarder do
    use GenEvent

    def handle_event(event, parent) do
      send parent, event
      {:ok, parent}
    end
  end

  alias SiteMonitor.Registry
  alias SiteMonitor.Db

  @test_site %Db.Site{name: "test", url: "http://example.com"}

  defp start_registry() do
    {:ok, db} = Db.start_link(Db.Repo)
    {:ok, event_manager} = GenEvent.start_link
    {:ok, sup} = SiteMonitor.Site.Supervisor.start_link
    {:ok, registry} = Registry.start_link(db, event_manager, sup)

    GenEvent.add_mon_handler(event_manager, Forwarder, self)

    registry
  end

  setup do
    {:ok, registry: start_registry}
  end

  test "spawns sites", %{registry: registry} do
    assert Registry.lookup(registry, @test_site.id) == :error

    {:ok, site_id, pid} = Registry.create(registry, @test_site)
    assert {:ok, ^pid} = Registry.lookup(registry, site_id)
  end

  test "removes sites on exit", %{registry: registry} do
    {:ok, site_id, pid} = Registry.create(registry, @test_site)
    {:ok, ^pid} = Registry.lookup(registry, site_id) 

    SiteMonitor.Site.stop(pid)

    assert_receive {:exit, site_id, ^pid}
    assert Registry.lookup(registry, site_id) == :error
  end

  test "removes sites on crash", %{registry: registry} do
    {:ok, site_id, pid} = Registry.create(registry, @test_site)
    {:ok, ^pid} = Registry.lookup(registry, site_id)

    Process.exit(pid, :shutdown)

    assert_receive {:exit, site_id, ^pid}
    assert Registry.lookup(registry, site_id) == :error
  end

  test "sends events on create and crash", %{registry: registry} do
    {:ok, site_id, pid} = Registry.create(registry, @test_site)

    assert_receive {:create, site_id, ^pid} 

    SiteMonitor.Site.stop(pid)

    assert_receive {:exit, site_id, ^pid}
  end

  test "monitors existing sites", %{registry: registry} do
    {:ok, site_id, pid} = Registry.create(registry, @test_site)

    Process.unlink(registry)
    Registry.stop(registry)

    registry = start_registry

    assert {:ok, _pid} = Registry.lookup(registry, site_id) 
  end
end
