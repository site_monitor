defmodule SiteMonitor.HttpCheckTest do
  use ExUnit.Case, async: true

  alias SiteMonitor.Db
  alias SiteMonitor.HttpCheck

  test "it checks an http url" do
    {:ok, site} = Db.create_site(SiteMonitor.Db, %Db.Site{name: "example", url: "http://example.com"})
    task = HttpCheck.async(SiteMonitor.Db, site.id)

    assert %{id: 1} = Task.await(task)
  end
end
