defmodule SiteMonitor.SiteTest do
	use ExUnit.Case, async: true
  
  setup do
    {:ok, db} = SiteMonitor.Db.start_link(SiteMonitor.Db.Repo)

    {:ok, event_manager} = GenEvent.start_link

    {:ok, db: db, events: event_manager}
  end

	test "starts", %{db: db, events: events} do
		assert {:ok, _} = SiteMonitor.Site.start_link(events, db, 1, 60000)
	end
end
